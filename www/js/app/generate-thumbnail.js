import SmartCrop from 'smartcrop'

export default function ( url, callback ) {
  var canvas = document.createElement('canvas')
  canvas.width = 256
  canvas.height = 256
  var context = canvas.getContext('2d')
  var imageObj = new Image()

  imageObj.onload = function() {
    try {
      SmartCrop.crop( imageObj, { width: canvas.width, height: canvas.width }, function( result ) {
        context.drawImage(imageObj, result.topCrop.x, result.topCrop.y, result.topCrop.width, result.topCrop.height, 0, 0, canvas.width, canvas.height)
        callback( canvas )
      })
    } catch( error ) {
      // displayErrorMessage( error.message )
    }
  };
  imageObj.src = url
}
