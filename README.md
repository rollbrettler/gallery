### Install SDK

```
android sdk
```

### Create Virtual device

```
android avd
```

### Create SD Card image

```
mksdcard -l mySdCard 1024M mySdCardFile.img
```

## Documentation

- https://cordova.apache.org/docs/en/dev/reference/cordova-plugin-file/index.html
- https://www.w3.org/TR/2012/WD-file-system-api-20120417/
- http://www.html5rocks.com/en/tutorials/file/filesystem/
