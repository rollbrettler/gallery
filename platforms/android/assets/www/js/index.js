import { template, sortBy } from 'lodash'
import generateThumbnail from './app/generate-thumbnail.js'
// var $ = window.jQuery

window.DEBUG = 1

window.templates = {
  folderContent: template(`<div data-href='<%- path %>' class='folder-card item mdl-card'>
              <div class='mdl-card__title mdl-card--expand'></div>
              <div class='mdl-card__actions'>
                <span class='demo-card-image__filename'><%- name %></span>
              </div>
            </div>`),
  image: template("<div class='item'><a data-href='<%- path %>'><img crossorigin='anonymous' src='<%= image %>' data-src='<%- path %>' alt='<%- name %>' /></a></div>")
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents()
    },

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false)
    },

    onDeviceReady: function() {
        window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem

        function onInitFs(fs) {
          displayFolderContent( cordova.file.externalRootDirectory )
        }

        window.requestFileSystem(window.PERSISTENT, 1024*1024*256, onInitFs, errorHandler)

    }
}

function errorHandler( error ) {
  var msg = ''

  switch ( error.code ) {
    case FileError.QUOTA_EXCEEDED_ERR:
      msg = 'QUOTA_EXCEEDED_ERR'
      break
    case FileError.NOT_FOUND_ERR:
      msg = 'NOT_FOUND_ERR'
      break
    case FileError.SECURITY_ERR:
      msg = 'SECURITY_ERR'
      break
    case FileError.INVALID_MODIFICATION_ERR:
      msg = 'INVALID_MODIFICATION_ERR'
      break
    case FileError.INVALID_STATE_ERR:
      msg = 'INVALID_STATE_ERR'
      break
    default:
      msg = 'Unknown Error: ' + error.message
      break
  }

  displayErrorMessage( msg )
}

function displayErrorMessage( message ) {
  document.getElementById('error').innerHTML = message
}

function displayDebugMessage( message ) {
  if (window.DEBUG === 1) {
    document.getElementById('debug').innerHTML += '<br/>' + message
  }
}

function displayFolderContent( path ) {
  window.resolveLocalFileSystemURL( path, function( fileEntry ) {
    displayDebugMessage( 'resolved name ' + fileEntry.toURL() )
    displayDebugMessage( 'is folder ' + fileEntry.isDirectory )
    var dirReader = fileEntry.createReader()
    var entries = []

    // Call the reader.readEntries() until no more results are returned.
    var readEntries = function() {
       dirReader.readEntries( function( results ) {
         displayDebugMessage( 'length' + results.length )

        if (!results.length) {
          listResults( sortBy(entries.sort(), function(entry) { return entry.name; }) )
        } else {
          entries = entries.concat( toArray( results ) )
          readEntries()
        }
      }, function(err) {
        displayErrorMessage( "readEntries error: " + err.code )
      })
    }

    readEntries() // Start reading dirs.
  }, errorHandler)
}

function toArray(list) {
  return Array.prototype.slice.call(list || [], 0)
}

function listResults(entries) {

  var fileList = document.querySelector('#file-list')
  var imageList = document.querySelector('#image-list')
  fileList.innerHTML = ''
  imageList.innerHTML = ''
  entries.forEach(function(entry, i) {

    if (entry.name.indexOf('.') === 0) {
      return
    }

    displayDebugMessage( 'entry toURL: ' + entry.toURL() )
    if ( entry.isDirectory ) {
      fileList.innerHTML += window.templates.folderContent({ path: entry.toURL(), name: entry.name })
    }
    if (entry.isFile && /\.(jpe?g|png|gif)$/i.test(entry.name) ) {
      var contentType = 'image/png'
      if(/\.(jpe?g)$/i.test(entry.name)) {
        contentType = 'image/jpeg'
      } else if (/\.(gif)$/i.test(entry.name)) {
         contentType = 'image/gif'
      }
      window.requestAnimationFrame(function() {
        generateThumbnail( entry.toURL(), function( canvas ) {
          imageList.innerHTML += window.templates.image({ path: entry.toURL(), name: entry.name, image: canvas.toDataURL(contentType) })
        })
      })
    }
    bindFolderClick()
  })
}

function folderClickEvent( event ) {
  event.preventDefault()
  displayFolderContent( event.target.dataset.href )
}

function bindFolderClick() {
  var folderLinks = document.querySelectorAll('.folder-card')
  displayDebugMessage( 'Number .folder of elements ' + folderLinks.length )
  for (var i = 0; i < folderLinks.length; i++) {
    folderLinks[i].removeEventListener('click', folderClickEvent)
    folderLinks[i].addEventListener('click', folderClickEvent)
  }
}

try {
  app.initialize()
} catch( error ) {
  displayErrorMessage( error.message )
}
